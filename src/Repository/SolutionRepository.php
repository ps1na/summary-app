<?php

namespace App\Repository;


use App\Entity\Solution;
use App\Entity\Summary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SolutionRepository extends ServiceEntityRepository
{
    /**
     * DegreeRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Solution::class);
    }

    /**
     * @param Summary $summary
     * @return mixed
     */
    public function getPositiveSolution(Summary $summary)
    {
        $solution = 1;

        return $this->createQueryBuilder('solution')
            ->andWhere('solution.solution = :solution')
            ->andWhere('solution.summary = :summary')
            ->setParameter('solution', $solution)
            ->setParameter('summary', $summary)
            ->getQuery()
            ->execute();
    }
}