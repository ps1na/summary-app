<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190527124054 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Демо-данные';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('INSERT INTO `summary`.`company` (`id`, `title`, `site`, `address`, `phone`) VALUES (NULL, \'Компания 1\', \'http://ya.ru\', \'г. Владимир\', \'111-22-33\')');
        $this->addSql('INSERT INTO `summary`.`company` (`id`, `title`, `site`, `address`, `phone`) VALUES (NULL, \'Компания 2\', \'http://test.ru\', \'г. Москва\', \'222-22-33\')');
        $this->addSql('INSERT INTO `summary`.`company` (`id`, `title`, `site`, `address`, `phone`) VALUES (NULL, \'Компания 3\', \'http://google.ru\', \'г. Калифорния\', \'333-22-33\')');
        $this->addSql('INSERT INTO `summary`.`company` (`id`, `title`, `site`, `address`, `phone`) VALUES (NULL, \'Компания 4\', \'http://go.ru\', \'г. Краснодар\', \'444-22-33\')');
        $this->addSql('INSERT INTO `summary`.`company` (`id`, `title`, `site`, `address`, `phone`) VALUES (NULL, \'Компания 5\', \'http://go.ru\', \'г. Нижний новгород\', \'555-22-33\')');

        $this->addSql('INSERT INTO `summary`.`summary` (`id`, `post_office`, `text_summary`, `create_at`, `update_at`) VALUES (NULL, \'Web-разработчик 1\', \'Текст резюме 1\', \'2019-05-26\', \'2019-05-26\')');
        $this->addSql('INSERT INTO `summary`.`summary` (`id`, `post_office`, `text_summary`, `create_at`, `update_at`) VALUES (NULL, \'Web-разработчик 2\', \'Текст резюме 2\', \'2019-05-26\', \'2019-05-26\')');
        $this->addSql('INSERT INTO `summary`.`summary` (`id`, `post_office`, `text_summary`, `create_at`, `update_at`) VALUES (NULL, \'Web-разработчик 3\', \'Текст резюме 3\', \'2019-05-26\', \'2019-05-26\')');
        $this->addSql('INSERT INTO `summary`.`summary` (`id`, `post_office`, `text_summary`, `create_at`, `update_at`) VALUES (NULL, \'Web-разработчик 4\', \'Текст резюме 4\', \'2019-05-26\', \'2019-05-26\')');

        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'1\', \'1\', \'2019-05-25\', \'1\')');
        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'1\', \'1\', \'2019-05-25\', \'2\')');
        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'1\', \'2\', \'2019-05-25\', \'3\')');
        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'1\', \'2\', \'2019-05-25\', \'4\')');
        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'1\', \'1\', \'2019-05-25\', \'5\')');

        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'2\', \'1\', \'2019-05-25\', \'1\')');
        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'2\', \'1\', \'2019-05-25\', \'2\')');
        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'2\', \'0\', \'2019-05-25\', \'3\')');

        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'3\', \'0\', \'2019-05-26\', \'1\')');
        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'3\', \'1\', \'2019-05-26\', \'2\')');
        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'3\', \'1\', \'2019-05-27\', \'3\')');
        $this->addSql('INSERT INTO `summary`.`solution` (`id`, `summary_id`, `solution`, `send_date`, `company_id`) VALUES (NULL, \'3\', \'1\', \'2019-05-27\', \'4\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
