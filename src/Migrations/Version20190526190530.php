<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190526190530 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Инициализация таблиц БД';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company (id INT NOT NULL, title LONGTEXT NOT NULL, site LONGTEXT NOT NULL, address LONGTEXT NOT NULL, phone LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE summary (id INT NOT NULL, post_office LONGTEXT NOT NULL, text_summary LONGTEXT NOT NULL, create_at DATE NOT NULL, update_at DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE solution (id INT NOT NULL, summary_id INT DEFAULT NULL, solution LONGTEXT NOT NULL, send_date DATE NOT NULL, INDEX IDX_9F3329DB2AC2D45C (summary_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE solution ADD CONSTRAINT FK_9F3329DB2AC2D45C FOREIGN KEY (summary_id) REFERENCES summary (id)');

        $this->addSql('ALTER TABLE company CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE summary CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE solution CHANGE id id INT AUTO_INCREMENT NOT NULL');

        $this->addSql('ALTER TABLE solution CHANGE solution solution LONGTEXT DEFAULT NULL');

        $this->addSql('ALTER TABLE solution ADD company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE solution ADD CONSTRAINT FK_9F3329DB979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_9F3329DB979B1AD6 ON solution (company_id)');

        $this->addSql('ALTER TABLE solution CHANGE solution solution INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE solution DROP FOREIGN KEY FK_9F3329DB2AC2D45C');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE summary');
        $this->addSql('DROP TABLE solution');
    }
}
