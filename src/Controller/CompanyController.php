<?php

namespace App\Controller;

use App\Dto\CompanyDto;
use App\Entity\Company;
use App\Form\CompanyForm;
use App\Repository\CompanyRepository;
use App\Repository\SolutionRepository;
use App\Repository\SummaryRepository;
use App\Service\CompanyService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CompanyController
 * @package App\Controller
 * @Route("/company", name="company_")
 */
class CompanyController extends AbstractController
{
    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * @var SummaryRepository
     */
    private $summaryRepository;

    /**
     * @var SolutionRepository
     */
    private $solutionRepository;

    /**
     * CompanyController constructor.
     * @param CompanyService $companyService
     * @param CompanyRepository $companyRepository
     * @param SummaryRepository $summaryRepository
     * @param SolutionRepository $solutionRepository
     */
    public function __construct(
        CompanyService $companyService,
        CompanyRepository $companyRepository,
        SummaryRepository $summaryRepository,
        SolutionRepository $solutionRepository
    )
    {
        $this->companyService = $companyService;
        $this->companyRepository = $companyRepository;
        $this->summaryRepository = $summaryRepository;
        $this->solutionRepository = $solutionRepository;
    }

    /**
     * @Route("/create", name="create")
     * @return Response
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(CompanyForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $companyDto = $form->getData();
                $this->companyService->createCompany($companyDto);
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->render('company/create.html.twig', [
                    'form' => $form->createView()
                ]);
            }
            return $this->redirectToRoute('company_index');
        }

        return $this->render('company/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     * @param Company $company
     * @param Request $request
     * @return Response
     */
    public function editAction(Company $company, Request $request)
    {
        $companyDto = new CompanyDto();
        $companyDto->setDto($company);
        $form = $this->createForm(CompanyForm::class, $companyDto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $companyDto = $form->getData();
                $this->companyService->editCompany($companyDto, $company);
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->render('company/edit.html.twig');
            }
            return $this->redirectToRoute('company_index');
        }

        return $this->render('company/edit.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/view/{id}", name="view")
     * @param Company $company
     * @param Request $request
     * @return Response
     */
    public function viewAction(Company $company, Request $request)
    {
        $companyDto = new CompanyDto();
        $companyDto->setDto($company);
        $form = $this->createForm(CompanyForm::class, $companyDto);
        $form->handleRequest($request);
        $form->remove('save');

        $solutions = $this->solutionRepository->findBy([
            'company' => $company
        ]);

        return $this->render('company/view.html.twig', [
            'form' => $form->createView(),
            'solutions' => $solutions
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @return Response
     */
    public function deleteAction(Company $company)
    {
        try {
            $this->companyService->deleteCompany($company);
        } catch (\Exception $exception) {
            $this->addFlash('error', $exception->getMessage());
            return $this->redirectToRoute('company_index');
        }

        return $this->redirectToRoute('company_index');
    }

    /**
     * @Route("/", name="index")
     * @return Response
     */
    public function listAction()
    {
        $companies = $this->companyRepository->findAll();
        $summaries = $this->summaryRepository->findAll();

//        foreach ($summaries as $item) {
//            dump($item->getSolution());
//        }exit();

        return $this->render('company/list.html.twig', [
            'companies' => $companies,
            'summaries' => $summaries
        ]);
    }
}