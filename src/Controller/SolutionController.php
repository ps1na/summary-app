<?php

namespace App\Controller;

use App\Dto\StatisticPositiveDto;
use App\Entity\Company;
use App\Entity\Solution;
use App\Entity\Summary;
use App\Form\SolutionForm;
use App\Repository\SolutionRepository;
use App\Repository\SummaryRepository;
use App\Service\SolutionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class SolutionController
 * @package App\Controller
 * @Route("/solution", name="solution_")
 */
class SolutionController extends AbstractController
{
    /**
     * @var SolutionService
     */
    private $solutionService;

    /**
     * @var SolutionRepository
     */
    private $solutionRepository;

    /**
     * @var SummaryRepository
     */
    private $summaryRepository;

    /**
     * SolutionController constructor.
     * @param SolutionService $solutionService
     * @param SolutionRepository $solutionRepository
     * @param SummaryRepository $summaryRepository
     */
    public function __construct(
        SolutionService $solutionService,
        SolutionRepository $solutionRepository,
        SummaryRepository $summaryRepository
    )
    {
        $this->solutionService = $solutionService;
        $this->solutionRepository = $solutionRepository;
        $this->summaryRepository = $summaryRepository;
    }

    /**
     * @Route("/create/{id_summary}/{id}", name="create")
     * @ParamConverter("summary", options={"mapping": {"id_summary": "id"}})
     * @param Summary $summary
     * @param Company $company
     * @return RedirectResponse
     */
    public function createAction(Summary $summary, Company $company)
    {
        try {
            $this->solutionService->createSolution($summary, $company);
        } catch (\Exception $exception) {
            $this->addFlash('error', $exception->getMessage());
        }

        return $this->redirectToRoute('company_index');
    }

    /**
     * @Route("/change-status/{id}", name="change")
     * @param Solution $solution
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeStatus(Solution $solution, Request $request)
    {
        $form = $this->createForm(SolutionForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $solutionDto = $form->getData();
                $this->solutionService->changeStatus($solutionDto, $solution);
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->render('solution/change_status.html.twig', [
                    'form' => $form->createView()
                ]);
            }
            return $this->redirectToRoute('company_view', [
                'id' => $solution->getCompany()->getId()
            ]);
        }

        return $this->render('solution/change_status.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/positive", name="positive")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function positiveStaticAction()
    {
        $summaries = $this->summaryRepository->findAll();
        $items = [];
        $itemsIds = [];
        $itemsCount = [];
        /**
         * @var Summary $summary
         */
        foreach ($summaries as $summary) {
            $allPositiveSolution = $this->solutionRepository->getPositiveSolution($summary);
            $statisticsPositive = new StatisticPositiveDto();
            $statisticsPositive->setCount(count($allPositiveSolution));
            $statisticsPositive->setIdSummary($summary->getId());
            $items[] = $statisticsPositive;
        }
//        FIXME: лишние действия , можно попроще сделать
        /**
         * @var $item StatisticPositiveDto
         */
        foreach ($items as $item) {
            $itemsIds[] = $item->getIdSummary();
            $itemsCount[] = $item->getCount();
        }
        $itemsIdsStr = implode(",", $itemsIds);
        $itemsCountStr = implode(",", $itemsCount);

        return $this->render('solution/positive.html.twig', [
            'items' => $items,
            'itemsIdsStr' => $itemsIdsStr,
            'itemsCountStr' => $itemsCountStr
        ]);
    }

    /**
     * @Route("/week", name="week")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getWeekData()
    {
        $solutions = $this->solutionRepository->findAll();
        $items = [];
        $itemsIds= [];
        $itemsCount = [];

        /**
         * @var Solution $solution
         */
        foreach ($solutions as $solution) {
            $date = $solution->getSendDate();
            $items[$date->format('D')] = count($this->solutionRepository->findBy([
                'solution' => 1,
                'sendDate' => $date
            ]));
        }

        foreach ($items as $key => $item) {
            $itemsIds[] = htmlspecialchars_decode('"'.$key.'"');
            $itemsCount[] = $item;
        }

        $itemsIdsStr = implode(",", $itemsIds);
        $itemsCountStr = implode(",", $itemsCount);

        return $this->render('solution/week.html.twig', [
            'itemsIdsStr' => $itemsIdsStr,
            'itemsCountStr' => $itemsCountStr
        ]);
    }
}