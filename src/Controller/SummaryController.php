<?php

namespace App\Controller;

use App\Dto\SummaryDto;
use App\Entity\Summary;
use App\Form\SummaryForm;
use App\Repository\SummaryRepository;
use App\Service\SummaryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SummaryController
 * @package App\Controller
 * @Route("/summary", name="summary_")
 */
class SummaryController extends AbstractController
{
    /**
     * @var SummaryService
     */
    private $summaryService;

    /**
     * @var SummaryRepository
     */
    private $summaryRepository;

    /**
     * SummaryController constructor.
     * @param SummaryService $summaryService
     * @param SummaryRepository $summaryRepository
     */
    public function __construct(SummaryService $summaryService, SummaryRepository $summaryRepository)
    {
        $this->summaryService = $summaryService;
        $this->summaryRepository = $summaryRepository;
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(SummaryForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $summaryDto = $form->getData();
                $this->summaryService->createSummary($summaryDto);
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->render('summary/create.html.twig', [
                    'form' => $form->createView()
                ]);
            }
            return $this->redirectToRoute('summary_index');
        }

        return $this->render('summary/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     * @param Summary $summary
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Summary $summary, Request $request)
    {
        $summaryDto = new SummaryDto();
        $summaryDto->setDto($summary);
        $form = $this->createForm(SummaryForm::class, $summaryDto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $summaryDto = $form->getData();
                $this->summaryService->editSummary($summaryDto, $summary);
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->render('summary/edit.html.twig', [
                    'form' => $form->createView()
                ]);
            }
            return $this->redirectToRoute('summary_index');
        }

        return $this->render('summary/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/view/{id}", name="view")
     * @param Summary $summary
     * @param Request $request
     * @return Response
     */
    public function viewAction(Summary $summary, Request $request)
    {
        $summaryDto = new SummaryDto();
        $summaryDto->setDto($summary);
        $form = $this->createForm(SummaryForm::class, $summaryDto);
        $form->handleRequest($request);
        $form->remove('save');

        return $this->render('summary/view.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @param Summary $summary
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Summary $summary)
    {
        try {
            $this->summaryService->deleteSummary($summary);
        } catch (\Exception $exception) {
            $this->addFlash('error', $exception->getMessage());
            return $this->redirectToRoute('summary_index');
        }

        return $this->redirectToRoute('summary_index');
    }

    /**
     * @Route("/", name="index")
     * @return Response
     */
    public function listAction()
    {
        $summaryies = $this->summaryRepository->findAll();

        return $this->render('summary/list.html.twig', [
            'summaryies' => $summaryies
        ]);
    }
}