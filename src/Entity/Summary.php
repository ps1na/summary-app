<?php

namespace App\Entity;

use App\Dto\SummaryDto;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="summary")
 * @ORM\Entity
 */
class Summary
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $postOffice;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $textSummary;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $createAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $updateAt;

    /**
     * @var Solution
     * @ORM\OneToMany(targetEntity="Solution", mappedBy="summary")
     */
    private $solution;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPostOffice()
    {
        return $this->postOffice;
    }

    /**
     * @param string $postOffice
     */
    public function setPostOffice($postOffice)
    {
        $this->postOffice = $postOffice;
    }

    /**
     * @return string
     */
    public function getTextSummary()
    {
        return $this->textSummary;
    }

    /**
     * @param string $textSummary
     */
    public function setTextSummary($textSummary)
    {
        $this->textSummary = $textSummary;
    }

    /**
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * @param \DateTime $createAt
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @param \DateTime $updateAt
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;
    }

    /**
     * @return Solution
     */
    public function getSolution()
    {
        return $this->solution;
    }

    /**
     * @param Solution $solution
     */
    public function setSolution($solution)
    {
        $this->solution = $solution;
    }

//    FIXME
    public function hasSolution()
    {
        return count($this->solution) > 0 ? true : false;
    }

//    FIXME
    public function thisCompany($companyId)
    {
        /**
         * @var Solution $solution
         */
        foreach ($this->solution as $solution) {
            if ($solution->getCompany()->getId() == $companyId) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param SummaryDto $summaryDto
     */
    public function setEntity(SummaryDto $summaryDto)
    {
        $this->setPostOffice($summaryDto->getPostOffice());
        $this->setTextSummary($summaryDto->getTextSummary());
    }
}