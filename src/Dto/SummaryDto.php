<?php

namespace App\Dto;

use App\Entity\Summary;

class SummaryDto
{
    /**
     * @var string
     */
    private $postOffice;

    /**
     * @var string
     */
    private $textSummary;

    /**
     * @var \DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     */
    private $updateAt;

    /**
     * @return string
     */
    public function getPostOffice()
    {
        return $this->postOffice;
    }

    /**
     * @param string $postOffice
     */
    public function setPostOffice($postOffice)
    {
        $this->postOffice = $postOffice;
    }

    /**
     * @return string
     */
    public function getTextSummary()
    {
        return $this->textSummary;
    }

    /**
     * @param string $textSummary
     */
    public function setTextSummary($textSummary)
    {
        $this->textSummary = $textSummary;
    }

    /**
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * @param \DateTime $createAt
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @param \DateTime $updateAt
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;
    }

    /**
     * @param Summary $summary
     */
    public function setDto(Summary $summary)
    {
        $this->setPostOffice($summary->getPostOffice());
        $this->setTextSummary($summary->getTextSummary());
        $this->setCreateAt($summary->getCreateAt());
        $this->setUpdateAt($summary->getUpdateAt());
    }
}