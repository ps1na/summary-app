<?php

namespace App\Dto;


class StatisticPositiveDto
{
    /**
     * @var int
     */
    private $count;

    /**
     * @var int
     */
    private $idSummary;

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getIdSummary()
    {
        return $this->idSummary;
    }

    /**
     * @param int $idSummary
     */
    public function setIdSummary($idSummary)
    {
        $this->idSummary = $idSummary;
    }
}