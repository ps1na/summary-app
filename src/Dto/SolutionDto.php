<?php

namespace App\Dto;


class SolutionDto
{
    /**
     * @var string
     */
    private $solution;

    /**
     * @return string
     */
    public function getSolution()
    {
        return $this->solution;
    }

    /**
     * @param string $solution
     */
    public function setSolution($solution)
    {
        $this->solution = $solution;
    }
}