<?php

namespace App\Dto;


use App\Entity\Company;

class CompanyDto
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $site;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $phone;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param string $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param Company $company
     */
    public function setDto(Company $company)
    {
        $this->setTitle($company->getTitle());
        $this->setSite($company->getSite());
        $this->setAddress($company->getAddress());
        $this->setPhone($company->getPhone());
    }
}