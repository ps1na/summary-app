<?php

namespace App\Service;

use App\Dto\SolutionDto;
use App\Entity\Company;
use App\Entity\Solution;
use App\Entity\Summary;
use Doctrine\ORM\EntityManagerInterface;

class SolutionService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CompanyService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Summary $summary
     * @param Company $company
     * @throws \Exception
     */
    public function createSolution(Summary $summary, Company $company)
    {
        $this->em->beginTransaction();
        try {
            $solution = new Solution();
            $solution->setCompany($company);
            $solution->setSummary($summary);
            $solution->setSendDate(new \DateTime());
            $this->em->persist($solution);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception('Ошибка ' . $exception);
        }
    }

    /**
     * @param SolutionDto $solutionDto
     * @param Solution $solution
     * @throws \Exception
     */
    public function changeStatus(SolutionDto $solutionDto, Solution $solution)
    {
        $this->em->beginTransaction();
        try {
            $solution->setSolution($solutionDto->getSolution());
            $this->em->persist($solution);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception('Ошибка ' . $exception);
        }
    }
}