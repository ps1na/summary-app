<?php

namespace App\Service;

use App\Dto\SummaryDto;
use App\Entity\Summary;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SummaryService
 * @package App\Service
 */
class SummaryService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * SummaryService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param SummaryDto $summaryDto
     * @throws \Exception
     */
    public function createSummary(SummaryDto $summaryDto)
    {
        $this->em->beginTransaction();
        try {
            $summary = new Summary();
            $summary->setEntity($summaryDto);
            $summary->setCreateAt(new \DateTime());
            $summary->setUpdateAt(new \DateTime());
            $this->em->persist($summary);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception('Ошибка ' . $exception);
        }
    }

    /**
     * @param SummaryDto $summaryDto
     * @param Summary $summary
     * @throws \Exception
     */
    public function editSummary(SummaryDto $summaryDto, Summary $summary)
    {
        $this->em->beginTransaction();
        try {
            $summary->setEntity($summaryDto);
            $summary->setUpdateAt(new \DateTime());
            $this->em->persist($summary);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception('Ошибка ' . $exception);
        }
    }

    /**
     * @param Summary $summary
     * @throws \Exception
     */
    public function deleteSummary(Summary $summary)
    {
        $this->em->beginTransaction();
        try {
            $this->em->remove($summary);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception($exception->getMessage());
        }
    }
}