<?php

namespace App\Service;


use App\Dto\CompanyDto;
use App\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CompanyService
 * @package App\Service
 */
class CompanyService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CompanyService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param CompanyDto $companyDto
     * @throws \Exception
     */
    public function createCompany(CompanyDto $companyDto)
    {
        $this->em->beginTransaction();
        try {
            $company = new Company();
            $company->setEntity($companyDto);
            $this->em->persist($company);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception('Ошибка ' . $exception);
        }
    }

    /**
     * @param CompanyDto $companyDto
     * @throws \Exception
     */
    public function editCompany(CompanyDto $companyDto, Company $company)
    {
        $this->em->beginTransaction();
        try {
            $company->setEntity($companyDto);
            $this->em->persist($company);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception('Ошибка ' . $exception);
        }
    }

    /**
     * @param Company $company
     * @throws \Exception
     */
    public function deleteCompany(Company $company)
    {
        $this->em->beginTransaction();
        try {
            $this->em->remove($company);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception($exception->getMessage());
        }
    }
}